using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuController : MonoBehaviour
{
    enum Screen
    {
        Main,
        Settings,
        LevelMenu,
        Pause,
        Win,
        GameOver,
        None
    }

    public CanvasGroup mainScreen;
    public CanvasGroup settingsScreen;
    public CanvasGroup levelMenuScreen;
    public CanvasGroup pauseScreen;
    public CanvasGroup winScreen;
    public CanvasGroup gameOverScreen;

    private void SetCurrentScreen(Screen screen)
    {
        Utility.SetCanvasGroupEnabled(mainScreen, screen == Screen.Main);
        Utility.SetCanvasGroupEnabled(settingsScreen, screen == Screen.Settings);
        Utility.SetCanvasGroupEnabled(levelMenuScreen, screen == Screen.LevelMenu);
        Utility.SetCanvasGroupEnabled(pauseScreen, screen == Screen.Pause);
        Utility.SetCanvasGroupEnabled(winScreen, screen == Screen.Win);
        Utility.SetCanvasGroupEnabled(gameOverScreen, screen == Screen.GameOver);
    }

    void Start()
    {
        SetCurrentScreen(Screen.Main);
    }

    public void StartNewGame()
    {
        OpenLevelMenu();
    }

    public void StartLevel1()
    {
        SceneManager.LoadScene("Level1");
    }

    public void StartLevel2()
    {
        SceneManager.LoadScene("Level2");
    }

    public void RestartLevel()
    {
        int buildIndex = SceneManager.GetActiveScene().buildIndex;
        SceneManager.LoadScene(buildIndex);
    }

    public void OpenLevelMenu()
    {
        SetCurrentScreen(Screen.LevelMenu);
    }

    public void CloseLevelMenu()
    {
        SetCurrentScreen(Screen.Main);
    }
    public void OpenSettings()
    {
        SetCurrentScreen(Screen.Settings);
    }

    public void CloseSettings()
    {
        SetCurrentScreen(Screen.Main);
    }

    public void OpenPauseMenu()
    {
        SetCurrentScreen(Screen.Pause);
    }

    public void ClosePauseMenu()
    {
        SetCurrentScreen(Screen.None);
    }

    public void OpenMainMenu()
    {
        SceneManager.LoadScene("Menu");
    }

    public void OpenWin()
    {
        SetCurrentScreen(Screen.Win);
    }

    public void OpenGameOver()
    {
        SetCurrentScreen(Screen.GameOver);
    }

    public void ExitGame()
    {
        Application.Quit();
    }
}
